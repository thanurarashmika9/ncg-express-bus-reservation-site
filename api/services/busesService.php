<?php
/**
 * Created by IntelliJ IDEA.
 * User: thanura_thilakarathna
 * Date: 12/1/18
 * Time: 12:46 AM
 */
require_once __DIR__.'/../db/dbConnection.php';
require_once __DIR__.'/../business/impl/busesBOImpl.php';

$method=$_SERVER['REQUEST_METHOD'];
$buses=new busesBOImpl();
switch ($method){
    case "POST":
        $operation=$_POST['operation'];
        break;
    case "GET":
        $operation=$_GET['operation'];
        switch ($operation){
            case "save":
                $plate_no=$_GET['plate'];
                $category=$_GET['category'];
                $seats=$_GET['seats'];
                $newBus=new buses($plate_no,$category,$seats);
                echo $buses->addBus($newBus);
                break;
            case "getAll":
                echo json_encode($buses->getAll());
                break;
        }
}