<?php
/**
 * Created by IntelliJ IDEA.
 * User: thanura_thilakarathna
 * Date: 11/27/18
 * Time: 3:35 PM
 */

require_once __DIR__.'/../db/dbConnection.php';
require_once  __DIR__.'/../business/impl/hiring_customerBOImpl.php';

$method=$_SERVER['REQUEST_METHOD'];
$hiring_customerBusiness=new hiring_customerBOImpl();
switch ($method){
    case "POST":
        $operation=$_POST['operation'];
        break;
    case "GET":
        $operation=$_GET['operation'];
        switch ($operation){
            case "add":
                $name=$_GET['name'];
                $password=$_GET['password'];
                $nic=$_GET['nic'];
                $email=$_GET['email'];
                $contact=$_GET['contact'];

                $newcustomerObject=new customer(0,$name,$password,$nic,$email,$contact);
                echo $hiring_customerBusiness->addHiringCustomer($newcustomerObject);
                break;
            case "getAll":
                echo json_encode($hiring_customerBusiness->getAll());
                break;
        }
}