<?php
/**
 * Created by IntelliJ IDEA.
 * User: thanura_thilakarathna
 * Date: 11/29/18
 * Time: 2:41 PM
 */

require_once __DIR__.'/../db/dbConnection.php';
require_once __DIR__.'/../business/impl/toursBOImpl.php';

$method=$_SERVER['REQUEST_METHOD'];
$tours=new toursBOImpl();
switch ($method){
    case "POST":
        $operation=$_POST['operation'];
        break;
    case "GET":
        $operation=$_GET['operation'];
        switch ($operation){
            case "getAll":
                echo json_encode($tours->getAll());
                break;
            case "save":
                $desc=$_GET['description'];
                $cat=$_GET['category'];
                $price=$_GET['price'];
                $date=$_GET['dates'];
                $days=$_GET['days'];
                $plate=$_GET['bus'];
                $tourObject=new tours(0,$desc,$cat,$price,$date,$days,$plate);
                echo $tours->save($tourObject);
                break;
            case "getLuxury":
                echo json_encode($tours->getLuxury());
                break;
            case "getSemiLuxury":
                echo json_encode($tours->getSemiLuxury());
                break;
        }
}