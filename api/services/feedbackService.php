<?php
/**
 * Created by IntelliJ IDEA.
 * User: thanura_thilakarathna
 * Date: 11/29/18
 * Time: 12:49 AM
 */
require_once __DIR__.'/../db/dbConnection.php';
require_once __DIR__.'/../business/impl/hiring_customerBOImpl.php';

$method=$_SERVER['REQUEST_METHOD'];
$customerBusiness=new hiring_customerBOImpl();
switch ($method){
    case "POST":
        $name=$_POST['names'];
        $email=$_POST['email'];
        $subject=$_POST['subject'];
        $message=$_POST['message'];
        $operation=$_POST['operation'];
        break;
    case "GET":
        $name=$_GET['names'];
        $email=$_GET['email'];
        $subject=$_GET['subject'];
        $message=$_GET['message'];
        $operation=$_GET['operation'];
        switch ($operation){
            case "save":
                $newFeedbackObject=new feedback($name,$email,$subject,$message);
                echo $customerBusiness->saveFeedback($newFeedbackObject);
                break;
        }
}