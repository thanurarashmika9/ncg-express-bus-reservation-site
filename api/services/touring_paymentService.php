<?php
/**
 * Created by IntelliJ IDEA.
 * User: thanura_thilakarathna
 * Date: 12/1/18
 * Time: 7:17 PM
 */

require_once __DIR__.'/../db/dbConnection.php';
require_once __DIR__.'/../business/impl/touring_paymentBOImpl.php';

$method=$_SERVER['REQUEST_METHOD'];
$payment=new touring_paymentBOImpl();
switch ($method){
    case "POST":
        $operation=$_POST['operation'];
        break;
    case "GET":
        $operation=$_GET['operation'];
        switch($operation){
            case "getPayment":
                echo json_encode($payment->getAll());
                break;
        }
}