<?php
/**
 * Created by IntelliJ IDEA.
 * User: thanura_thilakarathna
 * Date: 11/27/18
 * Time: 9:00 AM
 */

class category
{
    private $cat_id;
    private $category;

    /**
     * category constructor.
     * @param $cat_id
     * @param $category
     */
    public function __construct($cat_id, $category)
    {
        $this->cat_id = $cat_id;
        $this->category = $category;
    }

    /**
     * @return mixed
     */
    public function getCatId()
    {
        return $this->cat_id;
    }

    /**
     * @param mixed $cat_id
     */
    public function setCatId($cat_id): void
    {
        $this->cat_id = $cat_id;
    }

    /**
     * @return mixed
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * @param mixed $category
     */
    public function setCategory($category): void
    {
        $this->category = $category;
    }


}