<?php
/**
 * Created by IntelliJ IDEA.
 * User: thanura_thilakarathna
 * Date: 11/27/18
 * Time: 8:58 AM
 */

class buses
{
    private $plate_no;
    private $cat_id;
    private $seats;

    /**
     * buses constructor.
     * @param $plate_no
     * @param $cat_id
     * @param $seats
     */
    public function __construct($plate_no, $cat_id, $seats)
    {
        $this->plate_no = $plate_no;
        $this->cat_id = $cat_id;
        $this->seats = $seats;
    }

    /**
     * @return mixed
     */
    public function getPlateNo()
    {
        return $this->plate_no;
    }

    /**
     * @param mixed $plate_no
     */
    public function setPlateNo($plate_no): void
    {
        $this->plate_no = $plate_no;
    }

    /**
     * @return mixed
     */
    public function getCatId()
    {
        return $this->cat_id;
    }

    /**
     * @param mixed $cat_id
     */
    public function setCatId($cat_id): void
    {
        $this->cat_id = $cat_id;
    }

    /**
     * @return mixed
     */
    public function getSeats()
    {
        return $this->seats;
    }

    /**
     * @param mixed $seats
     */
    public function setSeats($seats): void
    {
        $this->seats = $seats;
    }


}