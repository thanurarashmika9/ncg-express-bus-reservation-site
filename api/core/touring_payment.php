<?php
/**
 * Created by IntelliJ IDEA.
 * User: thanura_thilakarathna
 * Date: 11/28/18
 * Time: 11:14 AM
 */

class touring_payment
{
    private $pid;
    private $tid;
    private $cid;
    private $payment;

    /**
     * touring_payment constructor.
     * @param $tid
     * @param $cid
     * @param $payment
     */
    public function __construct($pid,$tid, $cid, $payment)
    {
        $this->pid = $pid;
        $this->tid = $tid;
        $this->cid = $cid;
        $this->payment = $payment;
    }

    /**
     * @return mixed
     */
    public function getTid()
    {
        return $this->tid;
    }

    /**
     * @param mixed $tid
     */
    public function setTid($tid)
    {
        $this->tid = $tid;
    }

    /**
     * @return mixed
     */
    public function getCid()
    {
        return $this->cid;
    }

    /**
     * @param mixed $cid
     */
    public function setCid($cid)
    {
        $this->cid = $cid;
    }

    /**
     * @return mixed
     */
    public function getPayment()
    {
        return $this->payment;
    }

    /**
     * @param mixed $payment
     */
    public function setPayment($payment)
    {
        $this->payment = $payment;
    }


}