<?php
/**
 * Created by IntelliJ IDEA.
 * User: thanura_thilakarathna
 * Date: 11/30/18
 * Time: 8:19 PM
 */

class touring
{
    private $tourID;
    private $tod;
    private $cid;

    /**
     * touring constructor.
     * @param $tourID
     * @param $tod
     * @param $cid
     */
    public function __construct($tourID, $tod, $cid)
    {
        $this->tourID = $tourID;
        $this->tod = $tod;
        $this->cid = $cid;
    }

    /**
     * @return mixed
     */
    public function getTourID()
    {
        return $this->tourID;
    }

    /**
     * @param mixed $tourID
     */
    public function setTourID($tourID): void
    {
        $this->tourID = $tourID;
    }

    /**
     * @return mixed
     */
    public function getTod()
    {
        return $this->tod;
    }

    /**
     * @param mixed $tod
     */
    public function setTod($tod): void
    {
        $this->tod = $tod;
    }

    /**
     * @return mixed
     */
    public function getCid()
    {
        return $this->cid;
    }

    /**
     * @param mixed $cid
     */
    public function setCid($cid): void
    {
        $this->cid = $cid;
    }


}