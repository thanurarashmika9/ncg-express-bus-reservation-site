<?php
/**
 * Created by IntelliJ IDEA.
 * User: thanura_thilakarathna
 * Date: 11/29/18
 * Time: 8:14 PM
 */

class logindto
{
    private $cid;
    private $username;
    private $password;

    /**
     * logindto constructor.
     * @param $cid
     * @param $username
     * @param $password
     */
    public function __construct($cid, $username, $password)
    {
        $this->cid = $cid;
        $this->username = $username;
        $this->password = $password;
    }

    /**
     * @return mixed
     */
    public function getCid()
    {
        return $this->cid;
    }

    /**
     * @param mixed $cid
     */
    public function setCid($cid): void
    {
        $this->cid = $cid;
    }

    /**
     * @return mixed
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @param mixed $username
     */
    public function setUsername($username): void
    {
        $this->username = $username;
    }

    /**
     * @return mixed
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param mixed $password
     */
    public function setPassword($password): void
    {
        $this->password = $password;
    }


}