<?php
/**
 * Created by IntelliJ IDEA.
 * User: thanura_thilakarathna
 * Date: 11/29/18
 * Time: 7:57 PM
 */

class hires
{
    private $hid;
    private $plate_no;
    private $cat_id;
    private $cid;
    private $hdate;
    private $no_of_days;
    private $price;

    /**
     * hires constructor.
     * @param $hid
     * @param $plate_no
     * @param $cat_id
     * @param $cid
     * @param $hdate
     * @param $no_of_days
     * @param $price
     */
    public function __construct($hid, $plate_no, $cat_id, $cid, $hdate, $no_of_days, $price)
    {
        $this->hid = $hid;
        $this->plate_no = $plate_no;
        $this->cat_id = $cat_id;
        $this->cid = $cid;
        $this->hdate = $hdate;
        $this->no_of_days = $no_of_days;
        $this->price = $price;
    }

    /**
     * @return mixed
     */
    public function getHid()
    {
        return $this->hid;
    }

    /**
     * @param mixed $hid
     */
    public function setHid($hid): void
    {
        $this->hid = $hid;
    }

    /**
     * @return mixed
     */
    public function getPlateNo()
    {
        return $this->plate_no;
    }

    /**
     * @param mixed $plate_no
     */
    public function setPlateNo($plate_no): void
    {
        $this->plate_no = $plate_no;
    }

    /**
     * @return mixed
     */
    public function getCatId()
    {
        return $this->cat_id;
    }

    /**
     * @param mixed $cat_id
     */
    public function setCatId($cat_id): void
    {
        $this->cat_id = $cat_id;
    }

    /**
     * @return mixed
     */
    public function getCid()
    {
        return $this->cid;
    }

    /**
     * @param mixed $cid
     */
    public function setCid($cid): void
    {
        $this->cid = $cid;
    }

    /**
     * @return mixed
     */
    public function getHdate()
    {
        return $this->hdate;
    }

    /**
     * @param mixed $hdate
     */
    public function setHdate($hdate): void
    {
        $this->hdate = $hdate;
    }

    /**
     * @return mixed
     */
    public function getNoOfDays()
    {
        return $this->no_of_days;
    }

    /**
     * @param mixed $no_of_days
     */
    public function setNoOfDays($no_of_days): void
    {
        $this->no_of_days = $no_of_days;
    }

    /**
     * @return mixed
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param mixed $price
     */
    public function setPrice($price): void
    {
        $this->price = $price;
    }


}