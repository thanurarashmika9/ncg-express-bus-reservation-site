<?php
/**
 * Created by IntelliJ IDEA.
 * User: thanura_thilakarathna
 * Date: 11/28/18
 * Time: 11:13 AM
 */

class hiring_payment
{
    private $pid;
    private $hid;
    private $cid;
    private $price;
    private $payment;
    private $balance;

    /**
     * hiring_payment constructor.
     * @param $hid
     * @param $cid
     * @param $price
     * @param $payment
     * @param $balance
     */
    public function __construct($pid,$hid, $cid, $price, $payment, $balance)
    {
        $this->pid = $pid;
        $this->hid = $hid;
        $this->cid = $cid;
        $this->price = $price;
        $this->payment = $payment;
        $this->balance = $balance;
    }

    /**
     * @return mixed
     */
    public function getPid()
    {
        return $this->pid;
    }

    /**
     * @param mixed $pid
     */
    public function setPid($pid): void
    {
        $this->pid = $pid;
    }

    /**
     * @return mixed
     */
    public function getHid()
    {
        return $this->hid;
    }

    /**
     * @param mixed $hid
     */
    public function setHid($hid)
    {
        $this->hid = $hid;
    }

    /**
     * @return mixed
     */
    public function getCid()
    {
        return $this->cid;
    }

    /**
     * @param mixed $cid
     */
    public function setCid($cid)
    {
        $this->cid = $cid;
    }

    /**
     * @return mixed
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param mixed $price
     */
    public function setPrice($price)
    {
        $this->price = $price;
    }

    /**
     * @return mixed
     */
    public function getPayment()
    {
        return $this->payment;
    }

    /**
     * @param mixed $payment
     */
    public function setPayment($payment)
    {
        $this->payment = $payment;
    }

    /**
     * @return mixed
     */
    public function getBalance()
    {
        return $this->balance;
    }

    /**
     * @param mixed $balance
     */
    public function setBalance($balance)
    {
        $this->balance = $balance;
    }


}