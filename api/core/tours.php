<?php
/**
 * Created by IntelliJ IDEA.
 * User: thanura_thilakarathna
 * Date: 11/28/18
 * Time: 11:14 AM
 */

class tours
{
    private $tid;
    private $description;
    private $cat_id;
    private $price;
    private $tour_date;
    private $no_of_days;
    private $plate_no;

    /**
     * tours constructor.
     * @param $tid
     * @param $description
     * @param $cat_id
     * @param $price
     * @param $tour_date
     * @param $no_of_days
     * @param $plate_no
     */
    public function __construct($tid, $description, $cat_id, $price, $tour_date, $no_of_days, $plate_no)
    {
        $this->tid = $tid;
        $this->description = $description;
        $this->cat_id = $cat_id;
        $this->price = $price;
        $this->tour_date = $tour_date;
        $this->no_of_days = $no_of_days;
        $this->plate_no = $plate_no;
    }

    /**
     * @return mixed
     */
    public function getTid()
    {
        return $this->tid;
    }

    /**
     * @param mixed $tid
     */
    public function setTid($tid): void
    {
        $this->tid = $tid;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     */
    public function setDescription($description): void
    {
        $this->description = $description;
    }

    /**
     * @return mixed
     */
    public function getCatId()
    {
        return $this->cat_id;
    }

    /**
     * @param mixed $cat_id
     */
    public function setCatId($cat_id): void
    {
        $this->cat_id = $cat_id;
    }

    /**
     * @return mixed
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param mixed $price
     */
    public function setPrice($price): void
    {
        $this->price = $price;
    }

    /**
     * @return mixed
     */
    public function getTourDate()
    {
        return $this->tour_date;
    }

    /**
     * @param mixed $tour_date
     */
    public function setTourDate($tour_date): void
    {
        $this->tour_date = $tour_date;
    }

    /**
     * @return mixed
     */
    public function getNoOfDays()
    {
        return $this->no_of_days;
    }

    /**
     * @param mixed $no_of_days
     */
    public function setNoOfDays($no_of_days): void
    {
        $this->no_of_days = $no_of_days;
    }

    /**
     * @return mixed
     */
    public function getPlateNo()
    {
        return $this->plate_no;
    }

    /**
     * @param mixed $plate_no
     */
    public function setPlateNo($plate_no): void
    {
        $this->plate_no = $plate_no;
    }


}