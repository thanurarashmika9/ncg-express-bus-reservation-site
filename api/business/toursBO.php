<?php
/**
 * Created by IntelliJ IDEA.
 * User: thanura_thilakarathna
 * Date: 11/30/18
 * Time: 1:39 AM
 */

interface toursBO
{
    public function getAll():array;
    public function save(tours $tours):bool;
    public function getLuxury():array;
    public function getSemiLuxury():array;
}