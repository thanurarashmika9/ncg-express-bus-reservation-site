<?php
/**
 * Created by IntelliJ IDEA.
 * User: thanura_thilakarathna
 * Date: 11/28/18
 * Time: 12:09 PM
 */

interface hiring_customerBO
{
    public function addHiringCustomer(customer $customer):bool;
    public function saveFeedback(feedback $feedback):bool;
    public function searchbyId($name):object;
    public function login($username):logindto;
    public function getAll():array;
}