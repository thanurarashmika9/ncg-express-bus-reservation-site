<?php
/**
 * Created by IntelliJ IDEA.
 * User: thanura_thilakarathna
 * Date: 11/30/18
 * Time: 1:41 AM
 */

require_once __DIR__.'../busesBO.php';
require_once __DIR__.'/../../core/category.php';
require_once __DIR__.'/../../db/dbConnection.php';
require_once __DIR__.'/../../repo/impl/categoryRepoImpl.php';

class categoryBOImpl implements categoryBO
{

    private $categoryRepo;

    /**
     * categoryBOImpl constructor.
     */
    public function __construct()
    {
        $this->categoryRepo=new categoryRepoImpl();
    }


    public function getCategory(): array
    {
        $connection=(new dbConnection())->getConnection();
        $this->categoryRepo->setConnection($connection);
        return $this->categoryRepo->getCategory();
    }
}