<?php
/**
 * Created by IntelliJ IDEA.
 * User: thanura_thilakarathna
 * Date: 11/28/18
 * Time: 12:09 PM
 */

require_once __DIR__ . '/../../core/customer.php';
require_once  __DIR__.'/../hiring_customerBO.php';
require_once  __DIR__.'/../../db/dbConnection.php';
require_once __DIR__.'/../../repo/impl/hiring_customerRepoImpl.php';

class hiring_customerBOImpl implements  hiring_customerBO
{

    private $hiring_customerRepo;

    /**
     * hiring_customerBOImpl constructor.
     */
    public function __construct()
    {
        $this->hiring_customerRepo=new hiring_customerRepoImpl();
    }


    public function addHiringCustomer(customer $customer): bool
    {
        $connection=(new dbConnection())->getConnection();
        $this->hiring_customerRepo->setConnection($connection);
        return $this->hiring_customerRepo->addCustomer($customer);
    }

    public function saveFeedback(feedback $feedback): bool
    {
        $connection=(new dbConnection())->getConnection();
        $this->hiring_customerRepo->setConnection($connection);
        return $this->hiring_customerRepo->saveFeedback($feedback);
    }

    public function searchbyId($name): object
    {
        $connection=(new dbConnection())->getConnection();
        $this->hiring_customerRepo->setConnection($connection);
        return $this->hiring_customerRepo->searchbyId($name);
    }

    public function login($username): logindto
    {
        $connection=(new dbConnection())->getConnection();
        $this->hiring_customerRepo->setConnection($connection);
        return $this->hiring_customerRepo->login($username);
    }

    public function getAll(): array
    {
        $connection=(new dbConnection())->getConnection();
        $this->hiring_customerRepo->setConnection($connection);
        return $this->hiring_customerRepo->getAll();
    }
}