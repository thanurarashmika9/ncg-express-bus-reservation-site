<?php
/**
 * Created by IntelliJ IDEA.
 * User: thanura_thilakarathna
 * Date: 11/30/18
 * Time: 1:41 AM
 */

require_once __DIR__.'/../busesBO.php';
require_once  __DIR__.'/../../db/dbConnection.php';
require_once  __DIR__.'/../../repo/impl/busesRepoImpl.php';
require_once __DIR__.'/../../core/buses.php';

class busesBOImpl implements busesBO
{
    private $busesRepo;

    /**
     * busesBOImpl constructor.
     */
    public function __construct()
    {
        $this->busesRepo=new busesRepoImpl();
    }

    public function addBus(buses $buses):bool {
        $connection=(new dbConnection())->getConnection();
        $this->busesRepo->setConnection($connection);
        return $this->busesRepo->addBus($buses);
    }


    public function removeBus($plate_no): bool
    {
        $connection=(new dbConnection())->getConnection();
        $this->busesRepo->setConnection($connection);
        return $this->busesRepo->removeBus($plate_no);
    }

    public function updateBus(buses $buses): bool
    {
        $connection=(new dbConnection())->getConnection();
        $this->busesRepo->setConnection($connection);
        return $this->busesRepo->updateBus($buses);
    }

    public function searchBus($plate_no): buses
    {
        $connection=(new dbConnection())->getConnection();
        $this->busesRepo->setConnection($connection);
        return $this->busesRepo->searchBus($plate_no);
    }

    public function getAll(): array
    {
        $connection=(new dbConnection())->getConnection();
        $this->busesRepo->setConnection($connection);
        return $this->busesRepo->getAll();
    }

    public function gettoDate($date): array
    {
        $connection=(new dbConnection())->getConnection();
        $this->busesRepo->setConnection($connection);
        return $this->busesRepo->gettoDate($date);
    }
}