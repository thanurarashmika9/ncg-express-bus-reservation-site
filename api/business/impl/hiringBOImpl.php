<?php
/**
 * Created by IntelliJ IDEA.
 * User: thanura_thilakarathna
 * Date: 11/30/18
 * Time: 12:14 AM
 */

require_once __DIR__.'/../hiringBO.php';
require_once __DIR__.'/../../core/hires.php';
require_once __DIR__.'/../../db/dbConnection.php';
require_once __DIR__.'/../../repo/impl/hiringRepoImpl.php';

class hiringBOImpl implements hiringBO
{

    private $hiringRepo;

    /**
     * hiringBOImpl constructor.
     */
    public function __construct()
    {
        $this->hiringRepo=new hiringRepoImpl();
    }


    public function saveHire(hires $hires): bool
    {
        $connection=(new dbConnection())->getConnection();
        $this->hiringRepo->setConnection($connection);
        return $this->hiringRepo->saveHire($hires);
    }

    public function getAll(): array
    {
        $connection=(new dbConnection())->getConnection();
        $this->hiringRepo->setConnection($connection);
        return $this->hiringRepo->getAll();
    }
}