<?php
/**
 * Created by IntelliJ IDEA.
 * User: thanura_thilakarathna
 * Date: 11/30/18
 * Time: 1:41 AM
 */

require_once __DIR__.'/../hiring_paymentBO.php';
require_once __DIR__.'/../../repo/impl/hiring_paymentRepoImpl.php';
require_once __DIR__.'/../../db/dbConnection.php';
require_once __DIR__.'/../../core/hiring_payment.php';

class hiring_paymentBOImpl implements hiring_paymentBO
{

    private $hiring_paymentRepo;

    /**
     * hiring_paymentBOImpl constructor.
     */
    public function __construct()
    {
        $this->hiring_paymentRepo=new hiring_paymentRepoImpl();
    }


    public function getAll(): array
    {
        $connection=(new dbConnection())->getConnection();
        $this->hiring_paymentRepo->setConnection($connection);
        return $this->hiring_paymentRepo->getAll();
    }
}