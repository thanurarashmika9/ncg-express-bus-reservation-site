<?php
/**
 * Created by IntelliJ IDEA.
 * User: thanura_thilakarathna
 * Date: 11/30/18
 * Time: 1:42 AM
 */

require_once __DIR__.'/../toursBO.php';
require_once __DIR__.'/../../core/tours.php';
require_once __DIR__.'/../../db/dbConnection.php';
require_once __DIR__."/../../repo/impl/toursRepoImpl.php";

class toursBOImpl implements toursBO
{

    private $touringRepo;

    /**
     * toursBOImpl constructor.
     */
    public function __construct()
    {
        $this->touringRepo=new toursRepoImpl();
    }

    public function getAll(): array
    {
        $connection=(new dbConnection())->getConnection();
        $this->touringRepo->setConnection($connection);
        return $this->touringRepo->getAll();
    }


    public function save(tours $tours): bool
    {
        $connection=(new dbConnection())->getConnection();
        $this->touringRepo->setConnection($connection);
        return $this->touringRepo->savetour($tours);
    }

    public function getLuxury(): array
    {
        $connection=(new dbConnection())->getConnection();
        $this->touringRepo->setConnection($connection);
        return $this->touringRepo->getLuxury();
    }

    public function getSemiLuxury(): array
    {
        $connection=(new dbConnection())->getConnection();
        $this->touringRepo->setConnection($connection);
        return $this->touringRepo->getSemiLuxury();
    }
}