<?php
/**
 * Created by IntelliJ IDEA.
 * User: thanura_thilakarathna
 * Date: 11/30/18
 * Time: 1:41 AM
 */

require_once __DIR__.'/../touring_paymentBO.php';
require_once __DIR__.'/../../core/touring_payment.php';
require_once __DIR__.'/../../db/dbConnection.php';
require_once __DIR__.'/../../repo/impl/touring_paymentRepoImpl.php';

class touring_paymentBOImpl implements touring_paymentBO
{

    private $touring_paymentRepo;

    /**
     * touring_paymentBOImpl constructor.
     */
    public function __construct()
    {
        $this->touring_paymentRepo=new touring_paymentRepoImpl();
    }


    public function getAll(): array
    {
        $connection=(new dbConnection())->getConnection();
        $this->touring_paymentRepo->setConnection($connection);
        return $this->touring_paymentRepo->getAll();
    }
}