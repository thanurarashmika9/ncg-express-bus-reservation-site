<?php
/**
 * Created by IntelliJ IDEA.
 * User: thanura_thilakarathna
 * Date: 11/30/18
 * Time: 1:39 AM
 */

interface busesBO
{
    public function addBus(buses $buses):bool;
    public function removeBus($plate_no):bool;
    public function updateBus(buses $buses):bool;
    public function searchBus($plate_no):buses;
    public function getAll():array;
    public function gettoDate($date):array;

}