<?php
/**
 * Created by IntelliJ IDEA.
 * User: thanura_thilakarathna
 * Date: 11/27/18
 * Time: 8:46 AM
 */

class dbConnection
{
    private $host="127.0.0.1";
    private $username="root";
    private $password="84260899";
    private $database="NCGExpress";
    private $port="3306";

    private $connection;

    /**
     * dbConnection constructor.
     */
    public function __construct()
    {
        $this->connection=new mysqli($this->host,$this->username,$this->password,$this->database,$this->port);
        if($this->connection->connect_errno){
            echo $this->connection->connect_error;
            die();
        }
    }

    public function getConnection(){
        return $this->connection;
    }

}