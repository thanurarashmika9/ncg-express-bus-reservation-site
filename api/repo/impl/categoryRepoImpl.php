<?php
/**
 * Created by IntelliJ IDEA.
 * User: thanura_thilakarathna
 * Date: 11/30/18
 * Time: 8:19 AM
 */

require_once __DIR__.'/../categoryRepo.php';
require_once __DIR__.'/../../db/dbConnection.php';
require_once __DIR__.'/../../core/category.php';

class categoryRepoImpl implements categoryRepo
{

    private $connection;

    public function setConnection(mysqli $connection)
    {
        $this->connection = $connection;
    }

    public function getCategory(): array
    {
        $result = $this->connection->query("SELECT category FROM category");
        return $result->fetch_all();
    }
}