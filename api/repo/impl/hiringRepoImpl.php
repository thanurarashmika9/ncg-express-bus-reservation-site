<?php
/**
 * Created by IntelliJ IDEA.
 * User: thanura_thilakarathna
 * Date: 11/30/18
 * Time: 12:14 AM
 */

require_once __DIR__.'/../hiringRepo.php';
require_once __DIR__.'/../../db/dbConnection.php';
require_once __DIR__.'/../../core/hires.php';

class hiringRepoImpl implements hiringRepo
{

    private $connection;

    public function setConnection(mysqli $connection)
    {
        $this->connection=$connection;
    }

    public function saveHire(hires $hires): bool
    {
        $result=$this->connection->query("INSERT INTO hires VALUES('{$hires->getHid()}','{$hires->getPlateNo()}','{$hires->getCatId()}','{$hires->getCid()}','{$hires->getHdate()}','{$hires->getNoOfDays()}','{$hires->getPrice()}')");
        return $result>0;
    }

    public function getAll(): array
    {
        $result=$this->connection->query("Select * from hires");
        return $result->fetch_all();
    }
}