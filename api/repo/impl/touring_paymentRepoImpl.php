<?php
/**
 * Created by IntelliJ IDEA.
 * User: thanura_thilakarathna
 * Date: 11/30/18
 * Time: 8:20 AM
 */

require_once __DIR__.'/../../core/touring_payment.php';
require_once __DIR__.'/../../db/dbConnection.php';
require_once __DIR__.'/../touring_paymentRepo.php';

class touring_paymentRepoImpl implements touring_paymentRepo
{

    private $connection;

    public function setConnection(mysqli $mysqli)
    {
        $this->connection=$mysqli;
    }

    public function getAll(): array
    {
        $result=$this->connection->query("select * from touring_payment");
        return $result->fetch_all();
    }
}