<?php
/**
 * Created by IntelliJ IDEA.
 * User: thanura_thilakarathna
 * Date: 11/28/18
 * Time: 12:25 PM
 */

require_once __DIR__ . '/../../core/customer.php';
require_once  __DIR__.'/../hiring_customerRepo.php';
require_once  __DIR__.'/../../db/dbConnection.php';
require_once __DIR__ . '/../../core/logindto.php';

class hiring_customerRepoImpl implements hiring_customerRepo
{

    private $connection;

    public function setConnection(mysqli $connection)
    {
        $this->connection=$connection;
    }

    public function addCustomer(customer $customer): bool
    {
        $result=$this->connection->query("INSERT INTO customer VALUES('{$customer->getCid()}','{$customer->getName()}','{$customer->getPassword()}','{$customer->getNic()}','{$customer->getEmail()}','{$customer->getContact()}')");
        return $result>0;
    }

    public function saveFeedback(feedback $feedback): bool
    {
        $result=$this->connection->query("INSERT INTO feedback VALUES ('{$feedback->getName()}','{$feedback->getEmail()}','{$feedback->getSubject()}','{$feedback->getMessage()}')");
        return $result>0;
    }

    public function searchbyId($name): object
    {
        $result=$this->connection->query("SELECT cid FROM customer WHERE name='$name'");
        return $result;
    }

    public function login($username): logindto
    {
        $result=$this->connection->query("SELECT * FROM admin WHERE username='$username'");
        return $result->fetch_assoc();
    }

    public function getAll(): array
    {
        $result=$this->connection->query("Select * from customer");
        return $result->fetch_all();
    }
}
