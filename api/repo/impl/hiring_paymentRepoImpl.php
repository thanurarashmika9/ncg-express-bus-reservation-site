<?php
/**
 * Created by IntelliJ IDEA.
 * User: thanura_thilakarathna
 * Date: 11/30/18
 * Time: 8:19 AM
 */

require_once __DIR__.'/../hiring_paymentRepo.php';
require_once __DIR__.'/../../core/hiring_payment.php';
require_once __DIR__.'/../../db/dbConnection.php';

class hiring_paymentRepoImpl implements hiring_paymentRepo
{

    private $connection;

    public function setConnection(mysqli $mysqli)
    {
        $this->connection=$mysqli;
    }

    public function getAll(): array
    {
        $result=$this->connection->query("Select * from hiring_payment");
        return $result->fetch_all();
    }
}