<?php
/**
 * Created by IntelliJ IDEA.
 * User: thanura_thilakarathna
 * Date: 11/30/18
 * Time: 8:20 AM
 */

require_once __DIR__.'/../toursRepo.php';
require_once __DIR__.'/../../core/tours.php';
require_once __DIR__.'/../../db/dbConnection.php';

class toursRepoImpl implements toursRepo
{

    private $connection;

    public function setConnection(mysqli $mysqli)
    {
        $this->connection=$mysqli;
    }

    public function savetour(tours $tours): bool
    {
        $result=$this->connection->query("insert into tours values('{$tours->getTid()}','{$tours->getDescription()}','{$tours->getCatId()}','{$tours->getPrice()}','{$tours->getTourDate()}','{$tours->getNoOfDays()}','{$tours->getPlateNo()}')");
        return $result>0;
    }

    public function getAll(): array
    {
        $result=$this->connection->query("select * from tours");
        return $result->fetch_all();
    }

    public function getLuxury(): array
    {
        $result=$this->connection->query("select tid,description,price,tourDate,no_of_days from tours where cat_id=1");
        return $result->fetch_all();
    }

    public function getSemiLuxury(): array
    {
        $result=$this->connection->query("select tid,description,price,tourDate,no_of_days from tours where cat_i=2");
        return $result->fetch_all();
    }
}