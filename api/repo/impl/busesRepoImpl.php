<?php
/**
 * Created by IntelliJ IDEA.
 * User: thanura_thilakarathna
 * Date: 11/30/18
 * Time: 8:18 AM
 */

require_once __DIR__.'/../busesRepo.php';
require_once __DIR__.'/../../db/dbConnection.php';
require_once __DIR__.'/../../core/buses.php';

class busesRepoImpl implements busesRepo
{

    private $connection;

    public function setConnection(mysqli $connection)
    {
        $this->connection=$connection;
    }

    public function addBus(buses $buses): bool
    {
        $result=$this->connection->query("INSERT INTO buses VALUES('{$buses->getPlateNo()}','{$buses->getCatId()}','{$buses->getSeats()}')");
        return $result>0;
    }

    public function removeBus($plate_no): bool
    {
        $result=$this->connection->query("DELETE FROM buses WHERE plate_no=?",$plate_no);
        return $result>0;
    }

    public function updateBus(buses $buses): bool
    {
        $result=$this->connection->query("UPDATE buses SET cat_id=?,seats=? where plate_no=?",$buses->getCatId(),$buses->getSeats(),$buses->getPlateNo());
        return $result>0;
    }

    public function searchBus($plate_no): buses
    {
        $result=$this->connection->query("SELECT * FROM buses WHERE plate_no=?",$plate_no);
        return $result->fetch_assoc();
    }

    public function getAll(): array
    {
        $result=$this->connection->query("SELECT * FROM buses");
        return $result->fetch_all();
    }

    public function gettoDate($date): array
    {
        $result=$this->connection->query("select plate_no from hires where hdate='$date'");
        return $result->fetch_all();
    }
}