<?php
/**
 * Created by IntelliJ IDEA.
 * User: thanura_thilakarathna
 * Date: 11/28/18
 * Time: 12:24 PM
 */

require_once __DIR__ . '/../core/customer.php';
require_once __DIR__ . '/../core/feedback.php';

interface hiring_customerRepo
{
    public function setConnection(mysqli $connection);
    public function addCustomer(customer $customer):bool;
    public function saveFeedback(feedback $feedback):bool;
    public function searchbyId($name):object;
    public function login($username):logindto;
    public function getAll():array;
}