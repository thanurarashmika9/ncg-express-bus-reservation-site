<?php
/**
 * Created by IntelliJ IDEA.
 * User: thanura_thilakarathna
 * Date: 11/30/18
 * Time: 8:18 AM
 */

require_once __DIR__.'/../core/touring_payment.php';

interface touring_paymentRepo
{
    public function setConnection(mysqli $mysqli);
    public function getAll():array;
}