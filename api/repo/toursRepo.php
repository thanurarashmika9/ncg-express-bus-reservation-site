<?php
/**
 * Created by IntelliJ IDEA.
 * User: thanura_thilakarathna
 * Date: 11/30/18
 * Time: 8:18 AM
 */

require_once __DIR__.'/../core/tours.php';

interface toursRepo
{
    public function setConnection(mysqli $mysqli);
    public function savetour(tours $tours):bool;
    public function getAll():array;
    public function getLuxury():array;
    public function getSemiLuxury():array;
}