<?php
/**
 * Created by IntelliJ IDEA.
 * User: thanura_thilakarathna
 * Date: 11/30/18
 * Time: 8:16 AM
 */

interface categoryRepo
{
    public function setConnection(mysqli $connection);
    public function getCategory():array;
}