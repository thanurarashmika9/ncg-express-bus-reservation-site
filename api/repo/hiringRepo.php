<?php
/**
 * Created by IntelliJ IDEA.
 * User: thanura_thilakarathna
 * Date: 11/30/18
 * Time: 12:13 AM
 */

require_once __DIR__.'/../core/hires.php';

interface hiringRepo
{
    public function setConnection(mysqli $connection);
    public function saveHire(hires $hires):bool;
    public function getAll():array;
}