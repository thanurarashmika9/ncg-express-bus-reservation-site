<?php
/**
 * Created by IntelliJ IDEA.
 * User: thanura_thilakarathna
 * Date: 11/30/18
 * Time: 8:17 AM
 */

require_once __DIR__.'/../core/hiring_payment.php';

interface hiring_paymentRepo
{
    public function setConnection(mysqli $mysqli);
    public function getAll():array;
}