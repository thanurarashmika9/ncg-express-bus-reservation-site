$('#btnSignup').click(function () {
    var formdata=$('#customerForm').serialize();
    $.ajax({
        url:"api/services/customerService.php",
        method:"GET",
        async:true,
        data:formdata+ "&operation=add"
    }).done(function (resp) {
        alert(resp);
        // alert("Welcome to NCG Express->Successfully Saved as a new Customer");
    });
});

$('#btnContact').click(function () {
    var formdata=$('#contact-form').serialize();
    $.ajax({
        url:"api/services/feedbackService.php",
        method:"GET",
        async:true,
        data:formdata+ "&operation=save"
    }).done(function (resp) {
        alert("Thank you for the Feedback");
    });
});

$('#btnCheckSemiLuxury').click(function () {
    var formdata=$('#semi_Luxury_form').serialize();
    $.ajax({
        url:"api/services/hiringService.php",
        method:"GET",
        async:true,
        data:formdata+ "&operation=checkSemi"
    }).done(function (resp) {
        alert(resp);
    });
});