$('#updating_bus').click(function () {
    var formdata=$('#busForm').serialize();
    $.ajax({
        url:"api/services/busesService.php",
        method:"GET",
        async:true,
        data:formdata+ "&operation=save"
    }).done(function (resp) {
        alert(resp);
    });
});

$('#customerLoad').click(function () {
    $('#customerTbody').empty();
    $.ajax({
        url:"api/services/customerService.php",
        method:"GET",
        async: true,
        data:"&operation=getAll",
        dataType:"json"
    }).done(function (resp) {
        for(var i in resp){
            var customer=resp[i];
            var cid=customer[0];
            var name=customer[1];
            var nic=customer[2];
            var mail=customer[3];
            var contact=customer[4];
            var row="<tr><td>"+cid+"</td><td>"+name+"</td><td>"+nic+"</td><td>"+mail+"</td><td>"+contact+"</td></tr>";
            $('#customerTbody').append(row);
        }
    });
});

$('#busLoad').click(function () {
    $('#busesTbody').empty();
    $.ajax({
        url:"api/services/busesService.php",
        method:"GET",
        async:true,
        data:"&operation=getAll",
        dataType: "json"
    }).done(function (resp) {
        for(var i in resp){
            var bus=resp[i];
            var plate_no=bus[0];
            var cat_id=bus[1];
            var seats=bus[2];
            var row="<tr><td>"+plate_no+"</td><td>"+cat_id+"</td><td>"+seats+"</td></tr>";
            $('#busesTbody').append(row);
        }
    });
});

$('#tourLoad').click(function () {
    $('#toursTbody').empty();
    $.ajax({
        url:"api/services/touringService.php",
        method:"GET",
        async:true,
        data:"&operation=getAll",
        dataType:"json"
    }).done(function (resp) {
        for(var i in resp){
            var tour=resp[i];
            var tid=tour[0];
            var description=tour[1];
            var category=tour[2];
            var price=tour[3];
            var date=tour[4];
            var days=tour[5];
            var plate=tour[6];
            var row="<tr><td>"+tid+"</td><td>"+description+"</td><td>"+category+"</td><td>"+price+"</td><td>"+date+"</td><td>"+days+"</td><td>"+plate+"</td></tr>";
            $('#toursTbody').append(row);
        }
    });
});

$('#tour_update').click(function () {
    var formdata=$('#tourForm').serialize();
    $.ajax({
        url:"api/services/touringService.php",
        method:"GET",
        async:true,
        data:formdata+ "&operation=save"
    }).done(function (resp) {
        alert(resp);
    });
});

$('#tour_payment_Load').click(function () {
    $('#tours_paymentTbody').empty();
    $.ajax({
        url:"api/services/touring_paymentService.php",
        method:"GET",
        async:true,
        data:"&operation=getPayment",
        dataType:"json"
    }).done(function (resp) {
        for(var i in resp){
            var payments=resp[i];
            var pid=payments[0];
            var tid=payments[1];
            var cid=payments[2];
            var pay=payments[3];
            var row="<tr><td>"+pid+"</td><td>"+tid+"</td><td>"+cid+"</td><td>"+pay+"</td></tr>";
            $('#tours_paymentTbody').append(row);
        }
    })
});

$('#hireLoad').click(function () {
    $('#hiresTbody').empty();
    $.ajax({
        url:"api/services/hiringService.php",
        method:"GET",
        async:true,
        data:"&operation=loadHire",
        dataType:"json"
    }).done(function (resp) {
        for(var i in resp){
            var hires=resp[i];
            var hid=hires[0];
            var plate=hires[1];
            var cat_id=hires[2];
            var cid=hires[3];
            var date=hires[4];
            var days=hires[5];
            var price=hires[6];
            var row="<tr><td>"+hid+"</td><td>"+plate+"</td><td>"+cat_id+"</td><td>"+cid+"</td><td>"+date+"</td><td>"+days+"</td><td>"+price+"</td></tr>";
            $('#hiresTbody').append(row);
        }
    });
})

$('#loadHirePayment').click(function () {
    $('#hire_paymentTBody').empty();
    $.ajax({
        url:"api/services/hiring_paymentService.php",
        method:"GET",
        async:true,
        data:"&operation=loadHirePayments",
        dataType:"json"
    }).done(function (resp) {
        for(var i in resp){
            var payments=resp[i];
            var pid=payments[0];
            var hid=payments[1];
            var cid=payments[2];
            var price=payments[3];
            var pay=payments[4];
            var balance=payments[5];
            var row="<tr><td>"+pid+"</td><td>"+hid+"</td><td>"+cid+"</td><td>"+price+"</td><td>"+pay+"</td><td>"+balance+"</td></tr>";
            $('#hire_paymentTBody').append(row);
        }
    })
});