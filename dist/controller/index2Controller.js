$('#btnContact').click(function () {
    var formdata=$('#contact-form').serialize();
    $.ajax({
        url:"api/services/feedbackService.php",
        method:"GET",
        async:true,
        data:formdata+ "&operation=save"
    }).done(function (resp) {
        alert("Thank you for the Feedback");
    });
});

function load() {
    $('#sem_tourTBody').empty();
    $.ajax({
        url:"api/services/touringService.php",
        method: "GET",
        async: true,
        data:"&operation=getSemiLuxury",
        dataType:"json"
    }).done(function (resp) {
        alert(resp);
        for(var i in resp){
            var luxury=resp[i];
            var id=luxury[0];
            var destination=luxury[1];
            var category=luxury[2];
            var date=luxury[3];
            var days=luxury[4];
            var row="<tr><td>"+id+"</td><td>"+destination+"</td><td>"+category+"</td><td>"+date+"</td><td>"+days+"</td></tr>";
            $('#sem_tourTBody').append(row);
        }
    });

    $('#lux_tourTBody').empty();
    $.ajax({
        url:"api/services/touringService.php",
        method: "GET",
        async: true,
        data:"&operation=getLuxury",
        dataType:"json"
    }).done(function (resp) {
        for(var i in resp){
            var luxury=resp[i];
            var id=luxury[0];
            var destination=luxury[1];
            var category=luxury[2];
            var date=luxury[3];
            var days=luxury[4];
            var row="<tr><td>"+id+"</td><td>"+destination+"</td><td>"+category+"</td><td>"+date+"</td><td>"+days+"</td></tr>";
            $('#lux_tourTBody').append(row);
        }
    });
}