<?php
/**
 * Created by IntelliJ IDEA.
 * User: thanura_thilakarathna
 * Date: 11/29/18
 * Time: 10:04 PM
 */

?>

<section id="service" class="home-section text-center bg-gray">

    <div class="heading-about">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-lg-offset-2">
                    <div class="wow bounceInDown" data-wow-delay="0.4s">
                        <div class="section-heading">
                            <h2>Our Services</h2>
                            <i class="fa fa-2x fa-angle-down"></i>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-lg-2 col-lg-offset-5">
                <hr class="marginbot-50">
            </div>
        </div>
        <div class="row">
            <div class="col-md-3">
                <div class="wow fadeInLeft" data-wow-delay="0.2s">
                    <div class="service-box">
                        <div class="service-icon">
                            <img src="dist/img/icons/icons8-public-safety-50.png" alt="" />
                        </div>
                        <div class="service-desc">
                            <h5>Safe Transport</h5>
                            <p>We will provide you the best and safer transport services to fulfill your requirements</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="wow fadeInUp" data-wow-delay="0.2s">
                    <div class="service-box">
                        <div class="service-icon">
                            <img src="dist/img/icons/icons8-bus-50.png" alt="" />
                        </div>
                        <div class="service-desc">
                            <h5>New Buses</h5>
                            <p>Transportation is provided for you with our range of new Buses to uplift your travelling Experience</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="wow fadeInUp" data-wow-delay="0.2s">
                    <div class="service-box">
                        <div class="service-icon">
                            <img src="dist/img/icons/icons8-vip-50.png" alt="" />
                        </div>
                        <div class="service-desc">
                            <h5>Luxury Passenger Service</h5>
                            <p>We have Luxury features in Both Luxury and Semi_Luxury Buses to feel the ride </p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="wow fadeInRight" data-wow-delay="0.2s">
                    <div class="service-box">
                        <div class="service-icon">
                            <img src="dist/img/icons/icons8-staff-filled-50.png" alt="" />
                        </div>
                        <div class="service-desc">
                            <h5>Friendly Staff</h5>
                            <p>All our Staff members are Professionals and will provide you an Incredible Service</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- /Section: services -->
