<?php
/**
 * Created by IntelliJ IDEA.
 * User: thanura_thilakarathna
 * Date: 11/28/18
 * Time: 11:28 AM
 */
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>NCG Express Renting</title>
    <link rel="icon" href="dist/img/512x512bb.jpg"/>


    <!-- Bootstrap Core CSS -->
    <link href="dist/css/bootstrap.min.css" rel="stylesheet" type="text/css">

    <!-- Fonts -->
    <link href="dist/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="dist/css/animate.css" rel="stylesheet" />
    <!-- Squad theme CSS -->
    <link href="dist/css/style.css" rel="stylesheet">
    <link href="dist/color/default.css" rel="stylesheet">

    <!-- =======================================================
      Theme Name: Squadfree
      Theme URL: https://bootstrapmade.com/squadfree-free-bootstrap-template-creative/
      Author: BootstrapMade
      Author URL: https://bootstrapmade.com
    ======================================================= -->

</head>

<body id="page-top" data-spy="scroll" data-target=".navbar-custom">
<!-- Preloader -->
<div id="preloader">
    <div id="load"></div>
</div>

<?php
require "header.php";
?>

<div class="modal fade title1" id="developers">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" style="font-family:'typo' "><span style="color:orange">Sign Up</span></h4>
            </div>
            <div>
                <form id="customerForm">
                <input type="text" name="name" class="form-control" id="name" placeholder="Your Username" data-rule="minlen:4" data-msg="Invalid Username" />
                <input type="text" name="password" class="form-control" id="name" placeholder="Your Password" data-rule="minlen:4" data-msg="Invalid Password" />
                <input type="text" name="nic" class="form-control" id="name" placeholder="Your Identity" data-rule="minlen:4" data-msg="Invalid Identity" />
                <input type="text" name="email" class="form-control" id="name" placeholder="Your E-Mail" data-rule="minlen:4" data-msg="Invalid Email Address" />
                <input type="text" name="contact" class="form-control" id="name" placeholder="Your Contact Number" data-rule="minlen:4" data-msg="Invalid Contact Number" />
                </form>

                    <button type="button" id="btnSignup" class="btn btn-skin pull-right">Sign Up</button>
                    <button type="button" id="btnCancel" style="background: red" class="btn btn-skin pull-right" data-dismiss="modal">Cancel</button>

            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<div class="modal fade" id="myModal">
    <div class="modal-dialog">
        <div class="modal-content title1">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title title1"><span style="color:orange">Log In</span></h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal" action="login.php?q=index.php" method="POST">
                    <fieldset>



                        <div class="form-group">
                            <label class="col-md-3 control-label" for="email"></label>
                            <div class="col-md-6">
                                <input id="email" name="email" placeholder="Enter your email-id" class="form-control input-md" type="email">
                            </div>
                        </div>


                        <div class="form-group">
                            <label class="col-md-3 control-label" for="password"></label>
                            <div class="col-md-6">
                                <input id="password" name="password" placeholder="Enter your Password" class="form-control input-md" type="password">

                            </div>
                        </div>
                    </fieldset>
                </form>
            </div>
        </div>
    </div>
</div>

<!-- Section: intro -->
<section id="intro2" class="intro2">

    <div class="slogan">
        <h2>WELCOME TO <span class="text_color">NCG Express</span> </h2>
        <h4>WE WILL BE YOUR TRAVELLING PARTNER</h4>
    </div>
    <div class="page-scroll">
        <a href="#service" class="btn btn-circle">
            <i class="fa fa-angle-double-down animated"></i>
        </a>
    </div>
</section>
<!-- /Section: intro -->

<!-- Section: about -->
<section id="about" class="home-section text-center">
    <div class="heading-about">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-lg-offset-2">
                    <div class="wow bounceInDown" data-wow-delay="0.4s">
                        <div class="section-heading">
                            <h2>Rent Buses</h2>
                            <i class="fa fa-2x fa-angle-down"></i>
                            <div class="boxed-grey">
                                <h1>Luxury</h1>


                                <div id="sendmessage">Your message has been sent. Thank you!</div>
                                <div id="errormessage"></div>
                                <form id="contact-form" action="" method="post" role="form" class="contactForm">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="name">
                                                    Username</label>
                                                <input type="text" name="name" class="form-control" id="name" placeholder="Your Username" data-rule="minlen:4" data-msg="Invalid Username" />
                                                <div class="validation"></div>
                                            </div>
                                            <div class="form-group">
                                                <label for="subject">
                                                    From</label>
                                                <input type="date" class="form-control" name="rdate" id="subject" placeholder="Date" data-rule="minlen:4" data-msg="Invalid Date Format" />
                                                <div class="validation"></div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="name">
                                                    Contact</label>
                                                <input type="text" class="form-control" name="contact" id="subject" placeholder="Contact Number" data-rule="minlen:4" data-msg="Invalid Contact Number" />                                                <div class="validation"></div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="name">
                                                    no of days</label>
                                                <input type="number" class="form-control" name="days" id="subject" placeholder="No of Days" />                                                <div class="validation"></div>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <button type="submit" class="btn btn-skin pull-right" id="btnContactUs">
                                                Make Request</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            ############################################################################################
                            <div class="boxed-grey">
                                <h1>Semi-Luxury</h1>


                                <div id="sendmessage">Your message has been sent. Thank you!</div>
                                <div id="errormessage"></div>
                                <form id="contact-form" action="" method="post" role="form" class="contactForm">
                                </form>
                                <div class="row">
                                    <form id="semi_Luxury_form">
                                        <!--<input type="text" name="un" class="form-control" id="name" placeholder="Your Username" data-rule="minlen:4" data-msg="Invalid Username" />
                                        <input type="date" class="form-control" name="dt" id="subject" placeholder="Reserve Date" data-rule="minlen:4" data-msg="Invalid Date Format" />
                                        <input type="text" class="form-control" name="cnt" id="subject" placeholder="Contact Number" data-rule="minlen:4" data-msg="Invalid Contact Number" />
                                        <input type="number" class="form-control" name="dys" id="subject" placeholder="No of Days" data-rule="minlen:4" data-msg="Invalid Count" />-->
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="name">
                                                        Username</label>
                                                    <input type="text" name="un" class="form-control" id="name" placeholder="Your Username" data-rule="minlen:4" data-msg="Invalid Username" />
                                                    <div class="validation"></div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="subject">
                                                        From</label>
                                                    <input type="date" class="form-control" name="dt" id="subject" placeholder="Date" data-rule="minlen:4" data-msg="Invalid Date Format" />
                                                    <div class="validation"></div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="name">
                                                        Contact</label>
                                                    <input type="text" class="form-control" name="cnt" id="subject" placeholder="Contact Number" data-rule="minlen:4" data-msg="Invalid Contact Number" />                                                <div class="validation"></div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="name">
                                                        no of days</label>
                                                    <input type="number" class="form-control" name="dys" id="subject" placeholder="No of Days" />                                                <div class="validation"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                        <div class="col-md-12">
                                            <button type="submit" class="btn btn-skin pull-right" id="btnCheckSemiLuxury">
                                                Make Request</button>
                                        </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container">

        <div class="row">
            <div class="col-lg-2 col-lg-offset-5">
                <hr class="marginbot-50">
            </div>
        </div>
        <div class="row">
        </div>
    </div>
</section>
<!-- /Section: about -->


<?php
require "info.php";
?>


<?php
require "feedback.php";
?>


<footer>
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-lg-12">
                <div class="wow shake" data-wow-delay="0.4s">
                    <div class="page-scroll marginbot-30">
                        <a href="#intro" id="totop" class="btn btn-circle">
                            <i class="fa fa-angle-double-up animated"></i>
                        </a>
                    </div>
                </div>
                <p>&copy;NCG Express All rights reserved.</p>
                <div class="credits">
                    <!--
                      All the links in the footer should remain intact.
                      You can delete the links only if you purchased the pro version.
                      Licensing information: https://bootstrapmade.com/license/
                      Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/buy/?theme=Squadfree
                    -->
                    Designed by <a href="https://bootstrapmade.com/">Rush Web Delevopers</a>
                </div>
            </div>
        </div>
    </div>
</footer>

<!-- Core JavaScript Files -->
<script src="dist/js/jquery.min.js"></script>
<script src="dist/js/bootstrap.min.js"></script>
<script src="dist/js/jquery.easing.min.js"></script>
<script src="dist/js/jquery.scrollTo.js"></script>
<script src="dist/js/wow.min.js"></script>
<!-- Custom Theme JavaScript -->
<script src="dist/js/custom.js"></script>
<script src="dist/contactform/contactform.js"></script>
<script src="dist/controller/index1Controller.js"></script>

</body>

</html>

