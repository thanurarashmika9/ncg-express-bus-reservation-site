<?php
/**
 * Created by IntelliJ IDEA.
 * User: thanura_thilakarathna
 * Date: 11/28/18
 * Time: 11:25 AM
 */

session_start();

/*if(!isset($_SESSION['username'])){

}else{

}*/

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>NCG Express</title>
    <link rel="icon" href="dist/img/512x512bb.jpg" style="position: absolute;top: 4vh;left: 0;"/>


    <!-- Bootstrap Core CSS -->
    <link href="dist/css/bootstrap.min.css" rel="stylesheet" type="text/css">

    <!-- Fonts -->
    <link href="dist/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="dist/css/animate.css" rel="stylesheet" />
    <!-- Squad theme CSS -->
    <link href="dist/css/style.css" rel="stylesheet">
    <link href="dist/css/nn/mystyles.css" rel="stylesheet">
    <link href="dist/color/default.css" rel="stylesheet">

    <!-- =======================================================
      Theme Name: Squadfree
      Theme URL: https://bootstrapmade.com/squadfree-free-bootstrap-template-creative/
      Author: BootstrapMade
      Author URL: https://bootstrapmade.com
    ======================================================= -->

</head>

<body id="page-top" data-spy="scroll" data-target=".navbar-custom" style="background: #e3e3e3">
<!-- Preloader -->
<div id="preloader">
    <div id="load"></div>
</div>

<nav class="navbar navbar-custom navbar-fixed-top" role="navigation">
    <div class="container">
        <div class="navbar-header page-scroll">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-main-collapse">
                <i class="fa fa-bars"></i>
            </button>
            <a class="navbar-brand" href="index.html">
                <h1>NCG EXPRESS (pvt)LTD</h1>
                <img src="dist/img/ncg.png" style="position: absolute">
            </a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse navbar-right navbar-main-collapse">
            <ul class="nav navbar-nav">
                <li class="active"><a href="index.php">Home Page</a></li>
                </li>
            </ul>
        </div>
        <!-- /.navbar-collapse -->
    </div>
    <!-- /.container -->
</nav>

<div class="modal fade" id="myModal">
    <div class="modal-dialog">
        <div class="modal-content title1">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h1 class="modal-title title1"><span style="color:blue">New Buses</span></h1>
            </div>
            <div class="modal-body">
                        <label>Registration</label>
                        <label>Bus Category</label>
                        <label>Seating Capacity</label>
                        <form id="busForm">
                            <input id="plate" name="plate" placeholder="Plate Number" class="form-control input-md" type="text">
                            <input id="category" name="category" placeholder="Enter 1 for Luxury or 2 for Semi-Luxury" class="form-control input-md" type="number">
                            <input id="seats" name="seats" placeholder="Number of seats" class="form-control input-md" type="number">
                        </form>
                        <button class="btn btn-info" type="button" id="updating_bus">Add</button>
                        <button type="button" class="btn btn-danger" id="cancelbtn" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="new_tours">
    <div class="modal-dialog">
        <div class="modal-content title1">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h1 class="modal-title title1"><span style="color:blue">Add new Tours</span></h1>
            </div>
            <div class="modal-body">
                <form id="tourForm">
                    <label>Tour Description</label>
                    <input id="desc" name="description" placeholder="Tour Description" class="form-control input-md" type="text">

                    <label>Tour Category</label>
                    <input id="category" name="category" placeholder="Enter 1 for Luxury or 2 for Semi-Luxury" class="form-control input-md" type="number">

                    <label>Price</label>
                    <input id="price" name="price" placeholder="Tour Price" class="form-control input-md" type="number">

                    <label>Date</label>
                    <input id="date" name="dates" placeholder="Tour Date" class="form-control input-md" type="date">

                    <label>No.of Days</label>
                    <input id="days" name="days" placeholder="Number of Days" class="form-control input-md" type="number">

                    <label>Assigned Bus</label>
                    <input id="bus" name="bus" placeholder="Plate Number" class="form-control input-md" type="text">
                </form>
                <button class="btn btn-info" type="button" id="tour_update">Add</button>
                <button class="btn btn-danger" type="button" id="cancelbtn" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
</div>

<!--///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////-->

<section id="customers" class="home-section text-center">
    <div class="heading-contact">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-lg-offset-2">
                    <div class="wow bounceInDown" data-wow-delay="0.4s">
                        <div class="section-heading">
                            <h2>Customers</h2>
                            <i class="fa fa-2x fa-angle-down"></i>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container">

        <div class="row">
            <div class="col-lg-2 col-lg-offset-5">
                <hr class="marginbot-50">
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="boxed-grey">
                    <table class="table table-hover table-bordered">
                        <thead>
                        <tr>
                            <th>CID</th>
                            <th>Customer Name</th>
                            <th>NIC</th>
                            <th>E-mail</th>
                            <th>Contact</th>
                        </tr>
                        </thead>
                        <tbody id="customerTbody">

                        </tbody>
                    </table>
                    <button type="button" class="btn btn-success" id="customerLoad">Load Customers</button>
                </div>
            </div>
        </div>

    </div>
</section>


<section id="buses" class="home-section text-center">
    <div class="heading-contact">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-lg-offset-2">
                    <div class="wow bounceInDown" data-wow-delay="0.4s">
                        <div class="section-heading">
                            <h2>buses</h2>
                            <i class="fa fa-2x fa-angle-down"></i>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container">

        <div class="row">
            <div class="col-lg-2 col-lg-offset-5">
                <hr class="marginbot-50">
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="boxed-grey">
                    <table class="table table-hover table-bordered">
                        <thead>
                        <tr>
                            <th>Plate_No</th>
                            <th>Category[1=Luxury][2=Semi-Luxury]</th>
                            <th>Seats</th>
                        </tr>
                        </thead>
                        <tbody id="busesTbody">

                        </tbody>
                    </table>
                    <button class="btn btn-success" id="busLoad">Load Buses</button>
                    <button class="btn btn-info" id="addBus" data-toggle="modal" data-target="#myModal">New Bus</button>
                </div>
            </div>
        </div>

    </div>
</section>

<section id="tours" class="home-section text-center">
    <div class="heading-contact">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-lg-offset-2">
                    <div class="wow bounceInDown" data-wow-delay="0.4s">
                        <div class="section-heading">
                            <h2>tours</h2>
                            <i class="fa fa-2x fa-angle-down"></i>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container">

        <div class="row">
            <div class="col-lg-2 col-lg-offset-5">
                <hr class="marginbot-50">
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="boxed-grey">
                    <table class="table table-hover table-bordered">
                        <thead>
                        <tr>
                            <th>TID</th>
                            <th>Description</th>
                            <th>Category[1=Luxury][2=Semi-Luxury]</th>
                            <th>Price</th>
                            <th>Date</th>
                            <th>No.of Days</th>
                            <th>Plate_No</th>
                        </tr>
                        </thead>
                        <tbody id="toursTbody">

                        </tbody>
                    </table>
                    <button class="btn btn-success" id="tourLoad">Load Tours</button>
                    <button class="btn btn-info" id="tourLoad" data-toggle="modal" data-target="#new_tours">New Tour</button>
                </div>
            </div>
        </div>

    </div>
</section>

<section id="tourspayment" class="home-section text-center">
    <div class="heading-contact">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-lg-offset-2">
                    <div class="wow bounceInDown" data-wow-delay="0.4s">
                        <div class="section-heading">
                            <h2>tour payments</h2>
                            <i class="fa fa-2x fa-angle-down"></i>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container">

        <div class="row">
            <div class="col-lg-2 col-lg-offset-5">
                <hr class="marginbot-50">
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="boxed-grey">
                    <table class="table table-hover table-bordered">
                        <thead>
                        <tr>
                            <th>PID</th>
                            <th>TID</th>
                            <th>CID</th>
                            <th>Payment</th>
                        </tr>
                        </thead>
                        <tbody id="tours_paymentTbody">

                        </tbody>
                    </table>
                    <button class="btn btn-success" id="tour_payment_Load">Load Payments</button>
                </div>
            </div>
        </div>

    </div>
</section>

<!-- Section: contact -->
<section id="hires" class="home-section text-center">
    <div class="heading-contact">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-lg-offset-2">
                    <div class="wow bounceInDown" data-wow-delay="0.4s">
                        <div class="section-heading">
                            <h2>hires</h2>
                            <i class="fa fa-2x fa-angle-down"></i>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container">

        <div class="row">
            <div class="col-lg-2 col-lg-offset-5">
                <hr class="marginbot-50">
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="boxed-grey">
                    <table class="table table-hover table-bordered">
                        <thead>
                        <tr>
                            <th>HID</th>
                            <th>Plate_No</th>
                            <th>Category[1=Luxury][2=Semi-Luxury]</th>
                            <th>Price</th>
                            <th>Date</th>
                            <th>cid</th>
                        </tr>
                        </thead>
                        <tbody id="hiresTbody">

                        </tbody>
                    </table>
                    <button class="btn btn-success" id="hireLoad">Load Hires</button>
                </div>
            </div>
        </div>

    </div>
</section>
<!-- /Section: contact -->

<section id="hirepayments" class="home-section text-center">
    <div class="heading-contact">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-lg-offset-2">
                    <div class="wow bounceInDown" data-wow-delay="0.4s">
                        <div class="section-heading">
                            <h2>hire payments</h2>
                            <i class="fa fa-2x fa-angle-down"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container">

        <div class="row">
            <div class="col-lg-2 col-lg-offset-5">
                <hr class="marginbot-50">
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="boxed-grey">
                    <table class="table table-hover table-bordered">
                        <thead>
                        <tr>
                            <th>PID</th>
                            <th>HID</th>
                            <th>CID</th>
                            <th>Price</th>
                            <th>Advance</th>
                            <th>Balance</th>
                        </tr>
                        </thead>
                        <tbody id="hire_paymentTBody">

                        </tbody>
                    </table>
                    <button class="btn btn-success" id="loadHirePayment">Load Payments</button>
                    <button class="btn btn-info" id="loadHirePayment">Update Payment</button>
                    <div class="row">
                        <input id="cids" name="cid" placeholder="Customer ID" class="col-md-5" type="text" style="position: relative;left: 8vw;margin-top: 2vh;">
                        <input id="prices" name="price" placeholder="Hire Price" class="col-md-5" type="number" style="position:relative;margin-top: 2vh;">
                        <input id="advances" name="advance" placeholder="Advance Payment" class="col-md-5" type="number" style="position: relative;left: 8vw;">
                        <input id="balances" name="balance" placeholder="Balance Payment" class="col-md-5" type="number">
                    </div>
                </div>
            </div>
        </div>

    </div>
</section>

<footer>
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-lg-12">
                <div class="wow shake" data-wow-delay="0.4s">
                    <div class="page-scroll marginbot-30">
                        <a href="#intro" id="totop" class="btn btn-circle">
                            <i class="fa fa-angle-double-up animated"></i>
                        </a>
                    </div>
                </div>
                <p>&copy;NCG Express All rights reserved.</p>
                <div class="credits">
                    <!--
                      All the links in the footer should remain intact.
                      You can delete the links only if you purchased the pro version.
                      Licensing information: https://bootstrapmade.com/license/
                      Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/buy/?theme=Squadfree
                    -->
                    Designed by <a href="https://bootstrapmade.com/">Rush Web Delevopers</a>
                </div>
            </div>
        </div>
    </div>
</footer>

<!-- Core JavaScript Files -->
<script src="dist/js/jquery.min.js"></script>
<script src="dist/js/bootstrap.min.js"></script>
<script src="dist/js/jquery.easing.min.js"></script>
<script src="dist/js/jquery.scrollTo.js"></script>
<script src="dist/js/wow.min.js"></script>
<!-- Custom Theme JavaScript -->
<script src="dist/js/custom.js"></script>
<script src="dist/contactform/contactform.js"></script>
<script src="dist/controller/adminController.js"></script>
</body>

</html>

